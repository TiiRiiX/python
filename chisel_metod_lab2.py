def print_mas(mas): #Процедура печати системы
  for i in range (0, 3):
    print('{0}x1 + {1}x2 + {2}x3 = {3}'.format(a[i][0],a[i][1],a[i][2],a[i][3]))

a = [[0,0,0,0],[0,0,0,0],[0,0,0,0]]
f = open('MEGA/input2.txt')
for i in range(0,3):
  for j in range(0,4):
    a[i][j] = float(f.readline().replace('\n','')) #Ввод коофициентов системы из файла

print('Исходная система уравнений: ')
print_mas(a)
print()

for i in range(0,3):
  for j in range(0,4):
    a[i][j] = float(f.readline().replace('\n',''))

a = [[j*0.1 for j in i] for i in a]
print('Система с коофициентами меньше 1:')
print_mas(a)
print()

a[0][0] = 1 - abs(a[0][0])
a[0][1] = -a[0][1]
a[0][2] = -a[0][2]

a[1][0] = a[1][0]
a[1][1] = 1 - abs(a[1][1])
a[1][2] = a[1][2]
a[1][3] = -a[1][3]

a[2][0] = -a[2][0]
a[2][1] = -a[2][1]
a[2][2] = 1 - abs(a[2][2]) #Переформировываем систему под форумулы

print('Формулы для вычисления x1,x2,x3 на шагах итераций:')
print('x1 = {0}x1 + {1}x2 + {2}x3 + {3}'.format(a[0][0], a[0][1], a[0][2], a[0][3]))
print('x2 = {0}x1 + {1}x2 + {2}x3 + {3}'.format(a[1][0], a[1][1], a[1][2], a[1][3]))
print('x3 = {0}x1 + {1}x2 + {2}x3 + {3}'.format(a[2][0], a[2][1], a[2][2], a[2][3]))
print()

x1 = [a[i][3] for i in range(0,3)] #Берем исходные данные из свободных членов
x2 = [0,0,0]
eps = 0.00001
k = 0

print('============')
while(abs(x2[0]-x1[0])>eps or abs(x2[1]-x1[1])>eps or abs(x2[2]-x1[2])>eps):
  print('({3}) x1 = {0}, x2 = {1}, x3 = {2}'.format(x1[0],x1[1],x1[2],k))
  x2 = x1.copy()
  for i in range(0,3):
    x1[i] = a[i][0]*x2[0]+a[i][1]*x2[1]+a[i][2]*x2[2]+a[i][3]
  k+=1
	
f.close()
	
	


