import math

def f(x):
	return (x/2 + 2)/(math.sqrt(x**2 + 1))

def answer(i1, i2, m, n2):
	n1 = 10
	for i in range(1,m):
		n1 = n1 * n1
		n2 = n2 * n2
	return('Интеграл = {0}'.format(i2 + (n1 / (n2 - n1)) * (i2 - i1)))

eps = 10**(-4)
i1, i2 = 0, 0
n1, n2 = 10, 20
a, b = 0.8, 1.6
h1, h2 = (b - a) / n1, (b - a) / n2

print('Формула трапеции:')
m = 2
for i in range(0,n1 + 1):
	if (i == 0) or (i == 10):
		i1 = i1 + f(a + i * h1) / 2
	else:
		i1 = i1 + f(a + i * h1)
i1 = i1 * h1
while (abs(i1 - i2) >= eps):
	for i in range(0,n2 + 1):
		if (i == 0) or (i == n2):
			i2 = i2 + f(a + i * h2) / 2
		else:
			i2 = i2 + f(a + i * h2)
	i2 = i2 * h2
	n2 = n2 * 2
	h2 = (b - a) / n2
print(answer(i1, i2, m, n2))

print('Формула Симпсона:')
i1, i2 = 0, 0
m = 4
for i in range(0,n1 + 1):
	if ((i != 0) and (i != 10)) and ((i % 2) == 0):
		i1 = i1 + f(a + i * h1) * 2
	if (i % 2) != 0:
		i1 = i1 + f(a + i * h1) * 4
	if (i == 0) or (i == 10):
		i1 = i1 + f(a + i * h1)
i1 = i1 * h1 / 3
while (abs(i1 - i2 ) >= eps):
	for i in range(0,n2 + 1):
		if ((i != 0) and (i != n2)) and ((i % 2) == 0):
			i2 = i2 + f(a + i * h2) * 2
		if (i % 2) != 0:
			i2 = i2 + f(a + i * h2) * 4
		if (i == 0) or (i == n2):
			i2 = i2 + f(a + i * h2)
	i2 = i2 * h2 / 3
	n2 = n2 * 2
	h2 = (b - a) / n2
print(answer(i1, i2, m, n2))

print('Формула Гауса:')
m = 1
i1, i2 = 0, 0
n1, n2 = 4, 5
c1 = [0.34785, 0.65215, 0.65215, 0.34785]
t1 = [-0.86114, -0.33998, 0.33998, 0.86114]
c2 = [0.23693, 0.47863, 0.56889, 0.47863, 0.23693]
t2 = [-0.90618, -0.53847, 0, 0.53847, 0.90618]
for i in range(1,n1 + 1):
	i1 = i1 + c1[i - 1] * f((b + a) / 2 + (b - a) / 2 * t1[i - 1])
i1 = i1 * (b - a) / 2
for i in range(1,n2 + 1):
	i2 = i2 + c2[i - 1] * f((b + a) / 2 + (b - a) / 2 * t2[i - 1])
i2 = i2 * (b - a) / 2
print(answer(i1, i2, m, n2))