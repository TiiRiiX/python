import requests
city_id = 'Москва'
url = 'http://api.wunderground.com/api/06b18a6dc2fec81f/forecast/lang:RU/q/CA/{}.json'.format(city_id)
try:
  f = requests.get(url)
  b = f.json()['forecast']['simpleforecast']['forecastday']
  for i in range(0,len(b)):
    print('===={0}===='.format(b[i]['date']['weekday']))
    print(b[i]['conditions'].capitalize())
    print('Температура: {0}°..{1}°'.format(b[i]['low']['celsius'],b[i]['high']['celsius']))
    print('Влажность: {0}%'.format(b[i]['avehumidity']))
    print('Ветер: {0} м/с'.format(round(b[i]['avewind']['kph']*1000/3600,1)))
except:
  f = requests.get(url)
  print(f.json())
