# -*- coding: utf-8 -*- 

import requests

main_domain = 'https://spreadsheets.google.com/feeds/list/1vY4WVEVB0GMp8_nvorjBRObIfWaTn2nrOWEdfFTFkMs/od6/public/basic?alt=json'

page = requests.get(main_domain)


main_array = page.json()['feed']['entry']

output = '''--[[
========================
=====OOOOOO=O=====O=====
=====O======OO====O=====
=====O======O=O===O=====
=====OOO====O=====O=====
=====O======O===O=O=====
=====O======O====OO=====
=====O======O=====O=====
========================
=====OOOOOOOOOOOOOO=====
========================
=========PSUMAP=========
====COPYRIGHT FNIGHT====
========================
]]--

rooms_list = {\n'''

for line in main_array:
	number = ''
	new_number = ''
	if line['title']['$t'].find('(') == -1:
		number = line['title']['$t'] if line['title']['$t'].find('Row:') == -1 else ''
	else:
		number = line['title']['$t'][:line['title']['$t'].find('(')]
		new_number = line['title']['$t'][line['title']['$t'].find('(')+1:line['title']['$t'].find(')')]
	content = line['content']['$t']
	name = content[content.find('название:') + 10:content.find(',')] if content.find('название') != -1 else ''
	content = content[content.find(',') + 1:] if name != '' else content
	floor = content[content.find('этаж: ') + 6: content.find(',')]
	content = content[content.find(',') + 1:]
	corpus = content[content.find('корпус: ') + 8: content.find(',')]
	content = content[content.find(',') + 1:]
	ourId = content[content.find('id: ') + 4: content.find(',')] if content.find('тег') != -1 else content[content.find('id: ') + 4:]
	content = content[content.find(',') + 1:]
	tags = content[content.find('тег: ') + 5:] if content.find('тег') != -1 else ''
	output += '''\t{{
	number = "{0}", new_number = "{1}",
	name = "{2}", id = "{3}", tags = "{4}",
	x = 0, y = 0, z = {5}, corpus = {6},
	links = {{}}
	}},\n'''.format(number, new_number, name, ourId, tags, floor, corpus)

output += '}'

with open('rooms.lua', 'w', encoding='utf-8') as f:
	f.write(output)
	f.close()