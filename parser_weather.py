f = open('MEGA/59.xml')
main = f.read().replace('\n','').replace('\t','')
main = main[main.find('<FORECAST'):]
print('====Погода в Перми====')
day = []
for i in range(0,4):
	day.append(main[:main.find('</FORECAST>')])
	main = main[main.find('</FORECAST>')+11:]
for k in day:
	cur = k.split('"')
	key, key_word, mera = ['/><PRESSURE max=','/><TEMPERATURE max=','/><RELWET max=','/><WIND min=',' month='], ['Давление','Температура','Влажность'], [' мм рт.',' C','%']

	target = []
	target.append('--{0}.{1}.{2}-- ={3}:00='.format(cur[cur.index(key[4])-1],cur[cur.index(key[4])+1],cur[cur.index(key[4])+3],cur[cur.index(key[4])+5]))
	for i in range(0,len(key_word)):
		target.append('{0}: max = {1}{2}, min = {3}{4}'.format(key_word[i],cur[cur.index(key[i])+1],mera[i],cur[cur.index(key[i])+3],mera[i]))
	target.append('Ветер: min = {0} м/c, max = {1} м/с'.format(cur[cur.index(key[3])+1],cur[cur.index(key[3])+3]))
	print('\n'.join(target))
f.close()
