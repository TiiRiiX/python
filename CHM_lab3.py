def method_pro(kMatrix, free, second_pr):
	p = [0]*(N - 1)
	q = [0]*(N - 1)
	p[0] = - kMatrix[0][2] / kMatrix[0][1]
	q[0] = free[0] / kMatrix[0][1]
	i = 1
	while ( i < N - 2 ):		
		p[i] = - kMatrix[i][i + 2] / (kMatrix[i][i] * p[i - 1] + kMatrix[i][i + 1])
		q[i] = (free[i] - kMatrix[i][i] * q[i - 1]) / (kMatrix[i][i] * p[i - 1] + kMatrix[i][i + 1])
		i+=1
	i = 0
	while ( i < N - 2) :
		print('p[{0}] = {1}'.format(i,p[i]))
		print('q[{0}] = {1}'.format(i,q[i]))
		i+=1
	second_pr[N - 2] = q[N - 2]
	i = N - 2
	while (i >= 0):
		second_pr[i] = second_pr[i + 1] * p[i] + q[i]
		second_pr[i + 1] = second_pr[i]
		i-=1
	second_pr[0] = 0
	second_pr[N - 1] = 0
	i = 0
	while ( i < N ):
		print('second_pr[{0}] = {1}'.format(i, second_pr[i]))
		i+=1

N = 6
x = [0.15, 0.155, 0.16, 0.165, 0.17, 0.175]
y = [6.61659, 6.39989, 6.19658, 6.00551, 5.82558, 5.65583]
main_x = [0.1521, 0.1611, 0.1662, 0.1642, 0.1625]
kMatrix = [[0]*N]*N
free = [0]*N
step = [0]*N
second_pr = [0]*N
a, b, c, l, S = 0, 0, 0, 0, 0

i = 1
while (i < N):
	step[i - 1]= abs(x[i - 1] - x[i])
	i+=1

i = 0
while (i < N - 2):
	kMatrix[i][i] = step[i] / (step[i + 1] + step[i])
	kMatrix[i][i + 1] = 2
	kMatrix[i][i + 2] = step[i + 1] / (step[i + 1] + step[i])
	free[i] = 6 * (y[i] * (step[i] * (step[i + 1] * step[i]))**(-1) - y[i + 1] * (step[i + 1] * step[i])**(-1) + y[i + 2] * (step[i + 1] * (step[i + 1] + step[i]))**(-1))
	i+=1
kMatrix[0][0] = 0
kMatrix[N - 3][N - 1] = 0
i = 0
while (i < N - 2):
	j = 1
	while(j < N - 1):
		print('{0}, {1}'.format(kMatrix[i][j], free[i]))
		j+=1
	i+=1
method_pro(kMatrix, free, second_pr)
i = 0
while (i < N - 1):
	a = y[i]
	l = (second_pr[i + 1] - second_pr[i]) / (6 * step[i])
	c = second_pr[i] * 0.5
	b = (y[i + 1] - y[i]) / step[i] - step[i] * second_pr[i] / 3 - step[i] * second_pr[i + 1] / 6
	S = a + b * (main_x[i] - x[i]) + c * (main_x[i] - x[i])**2 + l * (main_x[i] - x[i])**3
	print('a = {0} b = {1} c = {2} l = {3}'.format(a,b,c,l))
	print('S({0}) = {1}'.format(main_x, S))
	i+=1