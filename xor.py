s = ' абвгдеёжзийклмнопрстуфхцчшщьыъэюя'
c, k = [int(i,16) for i in input().split(' ')], [int(j,16) for j in input().split(' ')]
t = [a ^ b  for a, b in zip(c, k)]
print([hex(i) for i in t], t, [s[i] for i in t], sep='\n')
