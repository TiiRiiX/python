import requests
import urllib.request
from time import sleep
import pickle
import subprocess

#get token from file with name
def GetToken(file):
	token_file = open(file)
	token = token_file.read().replace('\n','')
	token_file.close()
	return token

def UpdateUsers():
	user_base = []
	try:
		user_base_file = open('.users','br')
		user_base = pickle.load(user_base_file)
		user_base_file.close()
		print('base loaded')
	except:
		user_base_file = open('.users','bw')
		pickle.dump(user_base, user_base_file)
		user_base_file.close()
		print('new base')
	return user_base

def AddNewUser(user_base,idUser):
	user_base.append(idUser)
	user_base_file = open('.users','bw')
	pickle.dump(user_base, user_base_file)
	user_base_file.close()
	return user_base


def GetUpdates(lastId):
   getUpdates = 'https://api.telegram.org/bot' + TOKEN + '/getUpdates?offset=' + str(lastId)
   responce = requests.get(getUpdates)
   return responce.json()['result']

def SendMessage(text, idSender):
	sendMessage = 'https://api.telegram.org/bot' + TOKEN + '/sendMessage?chat_id=' + str(idSender) + '&text=' + text
	responce = requests.get(sendMessage)
	return responce.json()['result']

def GetIp():
	return str(urllib.request.urlopen('http://ident.me/').read())[2:-1]

def GetNmapScan():
	return str(subprocess.check_output(["nmap","-sn","192.168.2.1/24"])).replace('\\n','\n')[2:-1]

TOKEN = GetToken('/home/pi/.TOKEN')
lastUpdateId = 0
userBase = UpdateUsers()

dictAnswer = {
			'/ip' : GetIp(),
			'Я кушаю детей, пусти меня': 'Ты внутри, брат мой',
			'/scan' : GetNmapScan()
	}

#wait 60 second before start
sleep(60)
for user in userBase:
	SendMessage("I'm online, ip:{}".format(GetIp()), user)

while (True):
	sleep(1)
	try:
		newResponce = GetUpdates(lastUpdateId)
	except:
		print('connection error')
	if (len(newResponce) != 0):
		message = newResponce[0]['message']
		if (message['text'] == 'Я кушаю детей, пусти меня'):
			userBase = AddNewUser(userBase, message['from']['id'])
			print('User add ' + str(message['from']['id']))

		newAnswer = dictAnswer.get(message['text'])

		if (userBase.count(message['from']['id']) == 0):
			newAnswer = 'Access denied'
		if (newAnswer):
			SendMessage(newAnswer, message['from']['id'])
			print('send ' + message['text'])
		else:
			SendMessage('Unknow command', message['from']['id'])
			print('Unknow command ' + message['text'])
		lastUpdateId = newResponce[0]['update_id'] + 1
