import random
import time

def quickSort(a, first, last):
	i = first
	j = last
	x = a[(first + last) / 2]

	while(True):
		while(a[i] < x):
			i+=1
		while(a[j] > x):
			j-=1

		if (i <= j):
			if(a[i] > a[j]):
				temp = a[i]
				a[i] = a[j]
				a[j] = temp
			i+=1
			j-=1
		if(i > j):
			break

	if(i < last):
		quickSort(a, i, last)
	if(first < j):
		quickSort(a, first, j)

COUNT = 10

main_mas = [random.randint(1, COUNT*10) for i in range(1, COUNT+1)]
print(main_mas)
start = time.time()
quickSort(main_mas, 0, COUNT-1)
print('{0} elements were sorted'.format(COUNT))
print('time: {0}'.format(time.time()-start))
print(main_mas)
