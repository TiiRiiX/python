def f(x):
	return x*x*x+0.2*x*x+0.5*x-2

def f_p(x):
	return 3*x*x+0.4*x+0.5

def metd_half_slice():
	print('Метод половинного деления')
	first_a = a
	first_b = b
	c = (first_a + first_b)/2
	n = 0
	x = 0
	while (abs(first_b - first_a) > eps):
		if (f(first_a)*f(c) > 0):
			first_a = c
		else:
			first_b = c
		c = (first_a + first_b)/2
		n+=1
	x = (first_a+first_b)/2
	print('x = ',x)
	print('n = ',n)

def metd_hord():
	print('Метод хорд:')
	first_a = a
	first_b = b
	n = 0
	x = 0
	i = 1
	j = 2
	while (i < j):
		i+=1
		x = first_a - f(first_a)/(f(first_b) - f(first_a)) * (first_b - first_a)
		if (abs(f(x) - f(first_a)) > 0):
			first_a = x
			n+=1
			j+=1
		else:
			break
	print('x = ',x)
	print('n = ',n)

def metd_Niyton():
	print('Метод Ньютона(касательных)')
	first_b = b
	x = 0
	n = 0
	while (abs(f(first_b)/f_p(first_b) > eps)):
		x = first_b - (f(first_b)/f_p(first_b))
		first_b = x
		n+=1
	print('x = ',x)
	print('n = ',n)

def combo_metd():
	print('Комбинированный метод:')
	first_a = a
	first_b = b
	x = 0
	n = 0
	i = 1
	j = 2
	while (i < j):
		i+=1
		first_a = first_a - ((f(first_a)*(first_b - first_a))/(f(first_b) - f(first_a)))
		first_b = first_b - (f(first_b)/f_p(first_b))
		if (abs(first_b - first_a) > eps):
			j+=1
			n+=1
	print('x находится в [{0};{1}]'.format(first_a,first_b))
	print('n = ', n)

def metd_iter():	
	print('Метод итераций:')
	first_b = b
	x = 0
	n = 0
	i = 1
	j = 2
	while (i < j):
		i+=1
		x = -(first_b*first_b*first_b)/3 - 0.2*first_b*first_b/3 + 1.5*first_b/3 + 2/3
		if (abs((x - first_b)/first_b) > eps):
			first_b = x
			n+=1
			j+=1
		else:
			break
	print('x = ',x)
	print('n = ',n)

a = 1
b = 2
c = 0
eps = 0.00001
if (f(a)*f(b) < 0):
	metd_half_slice()
	metd_hord()
	metd_Niyton()
	combo_metd()
	metd_iter()
else:
	print('Не выполняются условия')
