import time
import math

start = time.time()

eps = 10**-25
x = 10

x = x*math.pi/180
sin = 0
temp = x
i = 1

while abs(temp) > eps:
  sin += temp
  temp *= -1 * x*x / (2*i) / (2*i+1)
  i += 1

print('sin({0}) = {1}'.format(x,sin))
print('time: {0}'.format(time.time()-start))
