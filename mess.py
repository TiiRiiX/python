# coding: utf8

import sys
import time
import random

def distance(a, b, d_min):
  n, m = len(a), len(b)
  if n > m:
    a, b = b, a
    n, m = m, n
  if n==0:
    return 100
  cff=float(m)/(max(n,0.001))
  current_row = range(n+1) # Keep current and previous row, not entire matrix
  for i in range(1, m+1):
    previous_row, current_row = current_row, [i]+[0]*n
    for j in range(1,n+1):
      add, delete, change = previous_row[j]+1, current_row[j-1]+1, previous_row[j-1]
      if a[j-1] != b[i-1]:
        change += 1
      current_row[j] = min(add, delete, change)
    if current_row[int(i//cff)]>d_min:
      return 100
  return current_row[n]
if __name__ == "__main__":
  key =''
  
  f = open("answer_database.bin",encoding="utf-8")
  base=[]
  for line in f:
    m=line.strip().split("||")
    base.append(m)
  f.close()

  while(True):
    key = input("Input:")
    d_min = 20
    ans = []
    t = time.time()
    for s1 in base:
      d = distance(key,s1[0],d_min)
      if (d<d_min):
        d_min = d
        ans = [s1[1]]
      elif d==d_min:
        ans.append(s1[1])
    t = time.time() - t
    print(u"Ответ: {0}({1})({2})".format(ans[random.randint(0,len(ans)-1)],d_min,round(t,2)))
    print(u"{0}".format(ans))
