import random

def print_mas(mas):
	for k in mas:
		print(' '.join(str(v) for v in k))

MAX_N = 50
draw_pole = [[" " for j in range(MAX_N)] for i in range(MAX_N)]
main_pole = [[0 for j in range(MAX_N)] for i in range(MAX_N)]
path_pole = [[0 for j in range(MAX_N)] for i in range(MAX_N)]
for i in range(MAX_N):
	for j in range(MAX_N):
		if (random.randint(0,10) < 3 or i == MAX_N - 1 or j ==MAX_N - 1 or i == 0 or j == 0):
			main_pole[i][j] = 1
			draw_pole[i][j] = "O"

print_mas(main_pole)
print_mas(draw_pole)